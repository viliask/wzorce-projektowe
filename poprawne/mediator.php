<?php

interface Mediator
{
    public function notify(object $sender, string $event): void;
}

class ConcreteMediator implements Mediator
{
    private $component1;
    private $component2;
    private $component3;

    private $temp1;
    private $temp2;
    private $temp3;

    private $array = array("1. Ćwiczenia zaliczone.", "2. Wykład (egzamin) zaliczony.",
        "3. Przedmiot zaliczony (wygrany konkurs).", "4. Wyjście z programu.");

    public function __construct(Component1 $c1, Component2 $c2, Component3 $c3)
    {
        $this->component1 = $c1;
        $this->component1->setMediator($this);
        $this->component2 = $c2;
        $this->component2->setMediator($this);
        $this->component3 = $c3;
        $this->component3->setMediator($this);
    }

    public function notify(object $sender, string $event): void
    {
        if ($event == "1") {
            if ($this->temp1 == 0){
                $this->array[0] = "1. Ćwiczenia zaliczone. X";
                $this->temp1 = 1;
            }
            else{
                $this->array[0] = "1. Ćwiczenia zaliczone.";
                $this->temp1 = 0;
            }
            $this->printArr();
        }
        if ($event == "2") {
            if ($this->temp2 == 0){
                $this->array[1] = "2. Wykład (egzamin) zaliczony. X";
                $this->temp2 = 1;
            }
            else{
                $this->array[1] = "2. Wykład (egzamin) zaliczony.";
                $this->temp2 = 0;
            }
            $this->component1->do1();
        }
        if ($event == "3") {
            if ($this->temp3 == 0){
                $this->array[2] = "3. Przedmiot zaliczony (wygrany konkurs). X";
                $this->temp3 = 1;
            }
            else{
                $this->array[2] = "3. Przedmiot zaliczony (wygrany konkurs).";
                $this->temp3 = 0;
            }
            $this->component2->do2();
        }
    }

    public function printArr()
    {
        foreach($this->array as $value) {
            print $value."\n";
        }
        print "\n";
    }
}

class BaseComponent
{
    protected $mediator;

    public function __construct(Mediator $mediator = null)
    {
        $this->mediator = $mediator;
    }

    function setMediator(Mediator $mediator): void
    {
        $this->mediator = $mediator;
    }
}

class Component1 extends BaseComponent
{
    public function do1(): void
    {
        $this->mediator->notify($this, "1");
    }
}

class Component2 extends BaseComponent
{
    public function do2(): void
    {
        $this->mediator->notify($this, "2");
    }
}

class Component3 extends BaseComponent
{
    public function do3(): void
    {
        $this->mediator->notify($this, "3");
    }
}

$c1 = new Component1();
$c2 = new Component2();
$c3 = new Component3();
$mediator = new ConcreteMediator($c1, $c2, $c3);

while (1) {
    $handle = fopen("php://stdin", "r");
    $line = fgets($handle);
    switch (trim($line)) {
        case "1":
            $c1->do1();
            break;
        case "2":
            $c2->do2();
            break;
        case "3":
            $c3->do3();
            break;
        case "4":
            echo "Koniec\n";
            exit;
    }
}