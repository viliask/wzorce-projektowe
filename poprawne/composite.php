<?php

abstract class Graphic
{
    protected $parent;

    public function setParent(Graphic $parent)
    {
        $this->parent = $parent;
    }

    public function getParent(): Graphic
    {
        return $this->parent;
    }

    public function add(Graphic $Graphic)
    {
    }

    public function remove(Graphic $Graphic)
    {
    }

    public function isPicture(): bool
    {
        return false;
    }

    public abstract function operation();
}

class Line extends Graphic
{
    public function operation()
    {
        return "Line";
    }
}

class Rectangle extends Graphic
{
    public function operation()
    {
        return "Rectangle";
    }
}

class Text extends Graphic
{
    public function operation()
    {
        return "Text";
    }
}


class Picture extends Graphic
{
    protected $children = [];

    public function add(Graphic $Graphic)
    {
        $this->children[] = $Graphic;
        $Graphic->setParent($this);
    }

    public function remove(Graphic $Graphic)
    {
        $this->children = array_filter($this->children, function ($child) use ($Graphic) {
            return $child == $Graphic;
        });
        $Graphic->setParent(null);
    }

    public function isPicture(): bool
    {
        return true;
    }

    public function operation()
    {
        $results = [];
        foreach ($this->children as $child) {
            $results[] = $child->operation();
        }

        return "Picture(" . implode("+", $results) . ")";
    }
}


print("\n\n");

$tree = new Picture();
$branch1 = new Picture();
$branch1->add(new Line());
$branch1->add(new Rectangle());
$branch2 = new Picture();
$branch2->add(new Text());
$branch2->add(new Line());
$branch2->add(new Rectangle());
$tree->add($branch1);
$tree->add($branch2);

$simple = new Line();

$tree->add($simple);

print("RESULT: " . $tree->operation());

print("\n\n");
