<?php

interface ObliczPodatek
{
    public function getPodatek(string $art, float $cena, float $ilosc);
}

class PodatekPolska implements ObliczPodatek
{
    public function getPodatek(string $art, float $cena, float $ilosc)
    {
        return 0.23 * $cena * $ilosc;
    }
}

class PodatekNiemcy implements ObliczPodatek
{
    public function getPodatek(string $art, float $cena, float $ilosc)
    {
        return 0.3 * $cena * $ilosc;
    }
}

class Konfiguracja
{
    public function region($region)
    {
        switch ($region) {
            case 'pl':
            default:
                return new PodatekPolska();
                break;
            case 'de':
                return new PodatekNiemcy();
                break;
        }
    }
}

class Zamowienie
{
    private $tax;
    private $coll = [];
    private $config;

    public function __construct()
    {
        $this->config = new Konfiguracja();
    }

    public function dodajArt($art, $cena, $ilosc)
    {
        $this->coll[] = [
            "art" => $art,
            "cena" => $cena,
            "ilosc" => $ilosc
        ];
    }

    public function getPodatek()
    {
        $sum = 0;

        foreach ($this->coll as $el) {
            $tax = $this->tax->getPodatek($el['art'], $el['cena'], $el['ilosc']);
            $sum += ($el['cena'] * $el['ilosc']);
            $sum += $tax;
        }
        return $sum;
    }

    public function setPodatek($region)
    {
        $this->tax = $this->config->region($region);
    }
}

$op = new Zamowienie();

$op->dodajArt('Maslo', 10.0, 1);
$op->dodajArt('Mleko', 1, 1);
$op->dodajArt('Kielbasa', 1000, 1);

$op->setPodatek('pl');

echo "Kwota z podatkiem to: ", $op->getPodatek(), "\n";

$op->setPodatek('de');

echo "Kwota z podatkiem to: ", $op->getPodatek();