<?php

class Singleton
{

    private static $obj;

    private final function __construct()
    {
        echo __CLASS__ . " initializes only once\n";
    }

    public static function getConn()
    {
        if (!isset(self::$obj)) {
            self::$obj = new Singleton();
        }
        return self::$obj;
    }

    private function __clone()
    {
    }

}


$obj1 = Singleton::getConn();
$obj2 = Singleton::getConn();


var_dump($obj1 == $obj2);
