<?php

interface Komponent
{
    public function drukuj();
}


class Potwierdzenie implements Komponent
{

    public function __construct()
    {
    }

    public function drukuj()
    {
            echo "Potwierdzenie\n";
    }

    public function config($c)
    {
        $this->Komponent = new Konfiguracja();
        $this->Komponent->pobierzPotwierdzenie($c);
    }
}

abstract class DekoratorPotwierdzenie implements Komponent
{

    protected $Komponent;

    #instanceof
    protected $body;

    public function __construct(Komponent $Komponent)
    {
        $this->Komponent = $Komponent;
    }

    public function drukuj(){
        if ($this->Komponent instanceof Komponent) {
            echo "POTWIERDZENIE\n";
        }
    }

}

class DekoratorNaglowka extends DekoratorPotwierdzenie
{

    public function drukuj()
    {
        echo "NAGLOWEK 1\n";
        $this->Komponent->drukuj();

    }

}

class DekoratorNaglowka2 extends DekoratorPotwierdzenie
{

    public function drukuj()
    {
        $this->Komponent->drukuj();
        echo "NAGLOWEK 2\n";
    }

}

class DekoratorStopki extends DekoratorPotwierdzenie
{

    public function drukuj()
    {
        $this->Komponent->drukuj();
        echo "STOPKA 1\n";
    }

}

class DekoratorStopki2 extends DekoratorPotwierdzenie
{

    public function drukuj()
    {
        $this->Komponent->drukuj();
        echo "STOPKA 2\n";
    }

}

class Konfiguracja extends Potwierdzenie
{

    public function pobierzPotwierdzenie($pobierzPotwierdzenie)
    {

        switch ($pobierzPotwierdzenie) {
            case 1:
                echo "\n\n";

                $lol = new Potwierdzenie();
                $lol = new DekoratorNaglowka($lol);
                $lol = new DekoratorStopki($lol);
                $lol = new DekoratorStopki2($lol);
                $lol->drukuj();
                break;
            case 2:
                echo "\n\n";
                $lol = new Potwierdzenie();
                $lol = new DekoratorNaglowka($lol);
                $lol = new DekoratorNaglowka2($lol);
                $lol = new DekoratorStopki2($lol);
                $lol->drukuj();
                break;
        }
    }
}

$Potwierdzenie = new Potwierdzenie();

$Potwierdzenie->config(1);


$Potwierdzenie2 = new Potwierdzenie();

$Potwierdzenie2->config(2);
//
//poprawic widoczność

//$Potwierdzenie = new Potwierdzenie();
//$Potwierdzenie = new DekoratorNaglowka($Potwierdzenie);
//$Potwierdzenie->drukuj();
//
//
//
//$Potwierdzenie = new Potwierdzenie();
//$Potwierdzenie = new DekoratorStopki($Potwierdzenie);
//$Potwierdzenie->drukuj();
//
//
//
//$Potwierdzenie = new Potwierdzenie();
//$Potwierdzenie = new DekoratorNaglowka($Potwierdzenie);
//$Potwierdzenie = new DekoratorStopki($Potwierdzenie);
//$Potwierdzenie->drukuj();




