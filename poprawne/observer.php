<?php

abstract class Observer
{
    abstract function update(Subject $ConcreteSubject_in);
}

class Subject
{
    function attach(Observer $observer_in)
    {
        $this->observers[] = $observer_in;
    }

    function detach(Observer $observer_in)
    {
        foreach ($this->observers as $okey => $oval) {
            if ($oval == $observer_in) {
                unset($this->observers[$okey]);
            }
        }
    }

    public function notify()
    {
        foreach ($this->observers as $obs) {
            $obs->update($this);
        }
    }
}


class ConcreteObserver extends Observer
{
    public function __construct()
    {
    }

    public function update(Subject $ConcreteSubject)
    {
        print("Powiadomienie\n");
        print("sub: " . $ConcreteSubject->getState())."\n";
        print("Koniec powiadomienia\n");
    }
}

class ConcreteSubject extends Subject
{
    private $subjectState = NULL;
    protected $observers = array();

    function __construct()
    {
    }

    function setState($new)
    {
        $this->subjectState = $new;
        $this->notify();
    }

    function getState()
    {
        return $this->subjectState;
    }
}

$spamer = new ConcreteSubject();

$subskrybent1 = new ConcreteObserver();
$subskrybent2 = new ConcreteObserver();

$spamer->attach($subskrybent1);
$spamer->attach($subskrybent2);
$spamer->setState("matrix, monk, 007");
$spamer->setState("new1, new2, new3");
$spamer->detach($subskrybent2);
$spamer->setState("new1, new2, new4");