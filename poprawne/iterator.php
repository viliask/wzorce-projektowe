<?php

class numIterator implements Iterator
{

    protected $i_position = 0;
    protected $numsCollection = array();

    public function __construct(numCollection $numsCollection)
    {
        $this->numsCollection = $numsCollection;
    }

    public function current()
    {
        return $this->numsCollection->getNum($this->i_position);
    }

    public function key()
    {
        return $this->i_position;
    }

    public function next()
    {
        do {
            $this->i_position++;
        } while ($this->zero());
        return !$this->end();
    }

    public function zero()
    {
        if (!$this->end()) {
            if ($this->current() == 0)
                return true;
            return false;
        }
        return false;
    }

    public function rewind()
    {
        $this->i_position = 0;
    }

    public function end()
    {
        if ($this->i_position < $this->numsCollection->getAmount()) {
            return false;
        }
        return true;
    }

    public function valid()
    {
        return !is_null($this->current());
    }
}

class numIterator2 extends numIterator implements Iterator
{

    public function next()
    {
        $this->i_position++;
    }
}

class numCollection implements IteratorAggregate
{
    private $a_nums = array();
    private $counter = 0;


    public function getIterator()
    {
        return new numIterator($this); // zmiana iteratora | bez zer
        //return new numIterator2($this); // zmiana iteratora  | z zerami
    }

    public function addNum($element)
    {
        $this->a_nums[] = $element;
        $this->counter++;
    }

    public function getNum($key)
    {
        if (isset($this->a_nums[$key])) {
            return $this->a_nums[$key];
        }
        return null;
    }

    public function is_empty()
    {
        return empty($this->a_nums);
    }

    public function getAmount()
    {
        return $this->counter;
    }
}

$numsCollection = new numCollection();
$numsCollection->addNum(666);
$numsCollection->addNum(3);
$numsCollection->addNum(9);
$numsCollection->addNum(0);
$numsCollection->addNum(0);
$numsCollection->addNum(12);
$numsCollection->addNum(16);

foreach ($numsCollection as $num) {
    echo($num);
    echo "\n";
}

// zmiana iteratora linijka ~78