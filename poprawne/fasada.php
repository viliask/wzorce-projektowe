<?php

class Vector
{
    private $_x;
    private $_y;
    private $_z;

    public function __construct($xx, $yy, $zz = null)
    {
        $this->_x = $xx;
        $this->_y = $yy;
        if ($zz != null)
            $this->_z = $zz;
    }

    public function add(Vector $vector)
    {
        $this->_x += $vector->getX();
        $this->_y += $vector->getY();
        $this->_z += $vector->getZ();
        return $this;
    }

    public function scale(Vector $vector)
    {
        $this->_x *= $vector->getX();
        $this->_y *= $vector->getY();
        $this->_z *= $vector->getZ();
        return $this;
    }

    public function __toString()
    {
        return sprintf("(%d, %s)",
            $this->_x,
            $this->_y . ($this->_z == null ? "" : ", " . $this->_z)
        );
    }

    public function getX()
    {
        return $this->_x;
    }

    public function getY()
    {
        return $this->_y;
    }

    public function getZ()
    {
        return $this->_z;
    }
}

$vec1 = new Vector(1, 2, 3);

$lol1 = new Vector(1, 3, 5);

echo $vec1->add($lol1);

echo "\n";

$vec2 = new Vector(2, 2);

$lol2 = new Vector(1, 3);

echo $vec2->add($lol2);
