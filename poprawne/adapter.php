<?php

class Figura
{
    private $_dlugosc = null;

    public function wyswietl()
    {
    }

    public function __construct($w)
    {
        $this->_dlugosc = $w;
    }

    public function getDl()
    {
        return $this->_dlugosc;
    }
}

class Linia extends Figura
{
    public function wyswietl()
    {
        echo "Linia długość: ", $this->getDl(), "\n";
    }
}

class Kwadrat extends Figura
{
    public function wyswietl()
    {
        echo "Kwadrat\n";
    }
}

class XXOkrag
{
    public function wyswietlaj()
    {
        echo "XXOkrag \n";
    }
}

class Okrag extends Figura
{
    public function wyswietl()
    {
        $adaptee = new XXOkrag();
        $adaptee->wyswietlaj();
    }
}

$linia = new Linia(666);
$linia->wyswietl();

$rotfl = new Okrag(5);
$rotfl->wyswietl(); // wyswietli "wyswietlaj()"
