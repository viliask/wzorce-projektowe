<?php

class SzablonZapytania
{
    public $zapytanie;

    public function __construct()
    {
    }

    public function utworzBD($param, $login, $password)
    {
        switch ($param) {
            case "SQL":
                $this->zapytanie = new ZapytanieSQLServer($login, $password);
                break;
            case "Oracle":
                $this->zapytanie = new ZapytanieOracle($login, $password);
                break;
            default:
                $this->zapytanie = new ZapytanieSQLServer($login, $password);
                break;
        }
        return $this->zapytanie;
    }

    public function wykonajZapytanie($content)
    {
        print("Tworze zapytanie\n");

        $this->zapytanie->formatujSelect($content);

        print("Wykonuje zapytanie\n");
    }

    protected function formatujSelect($content)
    {}

}

class ZapytanieSQLServer extends SzablonZapytania
{
    private $login, $password;

    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }


    protected function formatujSelect($content)
    {
        print("Wyslanie zapytania SQL: SELECT ". $content." FROM user1 dla: " . $this->login . "\n");
    }
}

class ZapytanieOracle extends SzablonZapytania
{
    private $email, $password;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }


    protected function formatujSelect($content)
    {
        print("Wyslanie zapytania Oracle: SELECT ". $content." FROM user1; dla: " . $this->email . "\n");
    }
}

$zapytanie = new SzablonZapytania;

$zapytanie->utworzBD("Oracle", "john_smith", "******");
$zapytanie->wykonajZapytanie("username");

print("\n\n");

$zzapytanie2 = new SzablonZapytania;

$zzapytanie2->utworzBD("SQL", "john_smith", "******");
$zzapytanie2->wykonajZapytanie("username");