<?php

interface Quadratic
{
    function calc($A, $B, $C);
}

class Proxy implements Quadratic
{

    function __construct()
    {
    }

    public function calc($A, $B, $C)
    {
        print("Proxy\n");

        if ($A == 0) {
            die("Not a quadratic equation\n");
        }

        $D = $B * $B - 4 * $A * $C;

        if ($D == 0) {
            echo "x = ", -$B / 2.0 / $A, "\n";
            die();
        }
        if ($D > 0) {
            echo "x1 = ", (-$B + sqrt($D)) / 2.0 / $A, "\n";
            echo "x2 = ", (-$B - sqrt($D)) / 2.0 / $A, "\n";
        } else {
            echo "x1 = (", -$B / 2.0 / $A, ", ", sqrt(-$D) / 2.0 / $A, ")\n";
            echo "x2 = (", -$B / 2.0 / $A, ", ", -sqrt(-$D) / 2.0 / $A, ")\n";
        }
    }
}

class Quad implements Quadratic
{
    private $obj;
    private $a, $b, $c;

    public function getC()
    {
        return $this->c;
    }

    public function getB()
    {
        return $this->b;
    }

    public function getA()
    {
        return $this->a;
    }

    public function __construct()
    {
    }

    public function calc($A, $B, $C)
    {
        if ($this->a == $A && $this->b == $B && $this->c == $C) {
            $this->obj = new Proxy();
            $this->obj->calc($A, $B, $C);
        } else {
            $this->a = $A;
            $this->b = $B;
            $this->c = $C;

            if ($A == 0) {
                die("Not a quadratic equation\n");
            }

            $D = $B * $B - 4 * $A * $C;

            if ($D == 0) {
                echo "x = ", -$B / 2.0 / $A, "\n";
                die();
            }
            if ($D > 0) {
                echo "x1 = ", (-$B + sqrt($D)) / 2.0 / $A, "\n";
                echo "x2 = ", (-$B - sqrt($D)) / 2.0 / $A, "\n";
            } else {
                echo "x1 = (", -$B / 2.0 / $A, ", ", sqrt(-$D) / 2.0 / $A, ")\n";
                echo "x2 = (", -$B / 2.0 / $A, ", ", -sqrt(-$D) / 2.0 / $A, ")\n";
            }
        }
    }

}

$kwd = new Quad();

$kwd->calc(-1, 3, 6);

print ("\n");

print ("a: ".$kwd->getA()."\n");

print ("\n");

$kwd->calc(-1, 3, 6);

print ("\n");

$kwd->calc(-1, 3, 7);