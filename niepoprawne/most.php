<?php
/**
 * Created by PhpStorm.
 * User: Ziemniaki
 * Date: 24.11.2018
 * Time: 09:56
 */

//////////////// abstrakcja /////////////////////////

abstract class Figura
{
    private $_biblioteka;
    protected $_imp;


    function __construct($bibl = null)
    {
        $this->_biblioteka = $bibl;
        if ('V2' == $bibl) {
            $this->_imp = new BibliotekaV2();
        } else {
            $this->_imp = new BibliotekaV1();
        }
    }

    public function rysuj()
    {
    }

    protected function rysujLinie()
    {
    }

    protected function rysujOkrag()
    {
    }


}

class Okrag extends Figura
{
    public function rysuj()
    {
        return $this->_imp->rysujOkrag();
    }
}

class Prostokat extends Figura
{
    public function rysuj()
    {
        return $this->_imp->rysujLinie();
    }
}

/////////////////////////// Implementacja ///////////////////////

abstract class Biblioteka
{

    public function rysujLinie()
    {
    }

    public function rysujOkrag()
    {
    }
}

class BibliotekaV1 extends Biblioteka
{
    public function rysujLinie()
    {
        echo "Instrukcje | Linia | BibliotekaV1\n";
        $bg = new BG1();

        $bg->rysujLinie();

    }

    public function rysujOkrag()
    {
        echo "Instrukcje | Okrag | BibliotekaV1\n";

        $bg = new BG1();

        $bg->rysujOkrag();
    }

}

class BibliotekaV2 extends Biblioteka
{
    public function rysujLinie()
    {
        echo "Instrukcje | Linia | BibliotekaV2\n";
        $bg = new BG2();

        $bg->rysujLinie();

    }

    public function rysujOkrag()
    {
        echo "Instrukcje | Okrag | BibliotekaV2\n";

        $bg = new BG2();

        $bg->rysujOkrag();
    }

}


class BG1 extends Biblioteka
{
    public function rysujLinie()
    {
        echo "Instrukcje | Linia | BG1\n";
    }

    public function rysujOkrag()
    {
        echo "Instrukcje | Okrag | BG1\n";
    }
}

class BG2 extends Biblioteka
{
    public function rysujLinie()
    {
        echo "Instrukcje | Linia | BG2\n";
    }

    public function rysujOkrag()
    {
        echo "Instrukcje | Okrag | BG2\n";
    }
}


$linia = new Prostokat();
$linia->rysuj();

$rotfl = new Okrag('V2');
$rotfl->rysuj();