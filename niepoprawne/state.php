<?php

class File
{
    private $fileState;

    public function __construct()
    {
    }

    public function getState()
    {
        return $this->fileState->giveState($this);
    }

    public function read()
    {
        if ($this->getState() == "open")
            print ("Czytam plik\n");
        else
            $this->fileState->fileClosed();
    }

    public function write()
    {
        if ($this->getState() == "open")
            print ("Edytuję plik\n");
        else
            $this->fileState->fileClosed();
    }

    public function open()
    {
        print ("Otwieram plik\n");
        $this->setState(new FileState());
        $this->fileState->fileOpened();
    }

    public function close()
    {
        print ("Zamykam plik\n");
        $this->setState(new FileState());
        $this->fileState->fileClosed();
    }

    public function setState($stateState_in)
    {
        $this->fileState = $stateState_in;
    }
}

class FileState
{
    private $state;

    public function giveState()
    {
        return $this->state;
    }

    public function fileOpened()
    {
        print "Plik otwarty – mogę czytać z/pisać do pliku\n";
        return $this->state = "open";
    }

    public function fileClosed()
    {
        print "Plik zamkniety – nie mogę czytać z/pisać do pliku\n";
        return $this->state = "closed";
    }
}
echo "\n";

$file = new File();

$file->open();
echo "\n";
$file->read();
echo "\n";
$file->write();
echo "\n";
$file->close();
echo "\n";
$file->read();